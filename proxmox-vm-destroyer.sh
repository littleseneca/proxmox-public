#!/bin/bash
# This simple script finds all virtual machines within a range and destroyes them. 
# No backup is made prior to blowing up the VM's so make sure you are cool with that first.  
# To use, set the below Limits section and set the UID of the range of virtual machines you want to destroy
# Then, simply run this script as root on your proxmox hypervisor.

# LIMITS #
qm_min_limit="0"
qm_max_limit="700"
# /LIMITS #

# Script #
qm_txt=$(qm list | awk '{ print $1}')
qm_array=( $qm_txt )
unset qm_array[0]
for i in "${qm_array[@]}"
    do
    if (( $qm_min_limit < $i < $qm_max_limit )); then   
        qm destroy $i
    fi
done
# /Script #