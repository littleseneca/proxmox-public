#!/bin/bash

# Instructions # 
# This Simple Script assumes three things. 

# First, it assumes that your template images exist as high numebr virtual IDs. #
# For example, my virtual machines range from ID 100 - ID 500, and my templates are in the ID 10000 - ID 20000 range. #
# This script assumes that you want your new template to be the last VM ID in your template list #
# For example, if your current last template ID is 10001, your new template ID will be 10002 #

# Second, it assumes that you you have appended the file path of the .qcow2 image as an argument to the script #
# For example, to deploy this script correctly, you would run it as follows: 
# #!  ./proxmox-template-builder.sh alma-8-template.qcow2  # 

# Third, it assumes that you have appended the name you want to use for this template as an argument to the script #
# For example, to deploy this script with a name for your template, you would run it as follows:
# #! ./proxmox-template-builder.sh alma-8-template.qcow2 alma-8-template

# /Instructions #

# Script #
current_template=$(qm list | tail -1 | sed 's/\|/ /'|awk '{print $1}')
new_template=$(expr $current_template + 1)
qm create ${new_template} --memory 2048 --net0 virtio,bridge=vmbr0
qm importdisk ${new_template} ./$1 local-lvm
qm set ${new_template} --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-${new_template}-disk-0
qm set ${new_template} --boot c --bootdisk scsi0
qm set ${new_template} --ide2 local-lvm:cloudinit
qm set ${new_template} --agent enabled=1
qm set ${new_template} --name="$2"
qm template ${new_template}
# /Script #